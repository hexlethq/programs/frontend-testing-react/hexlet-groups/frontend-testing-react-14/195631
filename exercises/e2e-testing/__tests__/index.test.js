/* eslint-disable */
// BEGIN
require('expect-puppeteer');
const faker = require('faker');

const appUrl = 'http://localhost:5000';
const appArticlesUrl = `${appUrl}/articles`;

const extractText = async (selector) => {
  const element = await page.$(selector);
  const text = await page.evaluate((element) => {
    return element.textContent || element.value;
  }, element);
  return text;
};

test('Index work', async () => {
  await page.goto(appUrl);
  await expect(page).toMatch('Welcome to a Simple blog!');
});

test('Articles page work', async () => {
  await page.goto(appArticlesUrl);
  await expect(page).toMatch('Articles');
});

test('Articles creating work', async () => {
  await page.goto(appArticlesUrl);
  await expect(page).toMatch('Articles');

  await page.waitForSelector('table');
  await page.click('[data-testid="article-create-link"]');
  await page.waitForSelector('form');

  const name = faker.lorem.word();
  await expect(page).toFillForm('form', {
    'article[name]': name,
  });
  await expect(page).toSelect('[name="article[categoryId]"]', '1');

  await page.click('[data-testid="article-create-button"]');
  await page.waitForSelector('table');

  await expect(page).toMatch(name);
});

test('Articles editing work', async () => {
  await page.goto(appArticlesUrl);
  await page.waitForSelector('table');

  const name = await extractText('tbody tr td:nth-child(2)');

  await page.click('[data-testid^="article-edit-link"]');
  await page.waitForSelector('form');

  const editingName = await extractText('#name');
  await expect(name).toBe(editingName);

  const newName = name + 'hello';
  await expect(page).toFillForm('form', {
    'article[name]': newName,
  });
  await page.click('[data-testid="article-update-button"]');
  await page.waitForSelector('table');

  await expect(page).toMatch(newName);
});

// END

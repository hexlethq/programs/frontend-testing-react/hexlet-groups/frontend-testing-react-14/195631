/* eslint-disable */
const puppeteer = require('puppeteer');
const faker = require('faker');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

const includes = async (selector, text) => {
  const isIncludes = await page.evaluate(
    (selector, text) => {
      const node = document.querySelector(selector);
      return node.innerText.includes(text);
    },
    selector,
    text
  );

  if (!isIncludes) {
    throw new Error('The document does not contain text');
  }

  return true;
};

const extractText = async (selector) => {
  const element = await page.$(selector);
  const text = await page.evaluate((element) => {
    return element.textContent || element.value;
  }, element);
  return text;
};

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  test('Index work', async () => {
    await page.goto(appUrl);
    await expect(page.waitForSelector('#title')).resolves.toBeTruthy();
  });

  test('Articles page work', async () => {
    await page.goto(appUrl);
    await page.waitForSelector('.nav-link[href="/articles"]');
    await page.click('.nav-link[href="/articles"]');

    await expect(page.waitForSelector('#articles')).resolves.toBeTruthy();
  });

  test('Articles creating work', async () => {
    await page.goto(appArticlesUrl);

    await page.waitForSelector('a[href="/articles/new"]');
    await page.click('a[href="/articles/new"]');

    await page.waitForSelector('form #name');
    await page.waitForSelector('form #category');

    const name = faker.lorem.word();
    await page.type('form #name', name);
    await page.select('form #category', '1');
    await page.click('form input[type="submit"]');

    await page.waitForSelector('#articles');
    await expect(includes('table', name)).resolves.toBe(true);
  });

  test('Articles editing work', async () => {
    await page.goto(appArticlesUrl);
    await page.waitForSelector('#articles');

    const name = await extractText('tbody tr td:nth-child(2)');

    await page.click('tbody a[href$="/edit"]');
    await page.waitForSelector('#edit-form');

    const editingName = await extractText('#name');

    await expect(name).toBe(editingName);

    const newName = editingName + 'hello';
    await page.type('#name', newName);
    await page.click('form input[type="submit"]');

    await page.waitForSelector('#articles');
    await expect(includes('table', newName)).resolves.toBe(true);
  });

  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});

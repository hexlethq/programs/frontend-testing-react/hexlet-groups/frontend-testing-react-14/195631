// @ts-check
// BEGIN
/* eslint-disable */

import 'expect-puppeteer';
import getNextId from '../lib/getNextId';

const appUrl = 'http://localhost:8080';

const createTask = async (text) => {
  await expect(page).toFillForm('form', { text });
  await page.click('[data-testid="add-task-button"]');
};

beforeEach(async () => {
  await page.goto(appUrl);
});

test('App works', async () => {
  await expect(page).toMatchElement('[data-testid="task-name-input"]');
  await expect(page).toMatchElement('[data-testid="add-task-button"]');
});

test('Task creating works', async () => {
  await createTask('task1');
  await createTask('task2');

  await expect(page).toMatch('task1');
  await expect(page).toMatch('task2');
});

test('Task removing works', async () => {
  const taskId = getNextId();

  await createTask('task1');
  await createTask('task2');

  await page.waitForSelector(`[data-testid="remove-task-${taskId}"]`);
  await page.click(`[data-testid="remove-task-${taskId}"]`);

  await expect(page).not.toMatch('task1');
  await expect(page).toMatch('task2');
});
// END

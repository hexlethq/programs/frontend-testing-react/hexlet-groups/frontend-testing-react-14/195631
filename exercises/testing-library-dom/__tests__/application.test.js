// @ts-check
/* eslint-disable */

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');

const run = require('../src/application');

const { screen } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

beforeEach(() => {
  const initHtml = fs
    .readFileSync(path.join('__fixtures__', 'index.html'))
    .toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
const createTask = (name) => {
  userEvent.type(screen.getByLabelText('New Task Name'), name);
  userEvent.click(screen.getByLabelText('Add Task'));
};

const createList = (name) => {
  userEvent.type(screen.getByLabelText('New List Name'), name);
  userEvent.click(screen.getByLabelText('Add List'));
};

test('App works', () => {
  expect(screen.getByText(/general/i)).toBeVisible();
  expect(
    document.querySelector('[data-container="tasks"]')
  ).toBeEmptyDOMElement();
});

test('Task creating works', () => {
  createTask('task1');
  createTask('task2');

  expect(screen.getByLabelText('New Task Name')).toHaveValue('');
  expect(screen.getByText('task1')).toBeVisible();
  expect(screen.getByText('task2')).toBeVisible();
});

test('List creating works', () => {
  createTask('task1');
  createList('New list');

  userEvent.click(screen.getByText('New list'));
  createTask('task2');

  expect(screen.queryByText('task1')).not.toBeInTheDocument();
  expect(screen.getByText('task2')).toBeVisible();
});
// END

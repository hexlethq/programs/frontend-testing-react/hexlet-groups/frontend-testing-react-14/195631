const { strict: assert } = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const array = [1, [2, 3], [4, [5, 6, [7], [[]]]]];

assert.deepEqual(flattenDepth([]), []);
assert.deepEqual(flattenDepth(array), [1, 2, 3, 4, [5, 6, [7], [[]]]]);
assert.deepEqual(flattenDepth(array, 3), [1, 2, 3, 4, 5, 6, 7, []]);
assert.deepEqual(flattenDepth(array, 10), [1, 2, 3, 4, 5, 6, 7]);

assert.deepEqual(flattenDepth(array, -2), array);
assert.deepEqual(flattenDepth(array, 'invalid'), array);
// END

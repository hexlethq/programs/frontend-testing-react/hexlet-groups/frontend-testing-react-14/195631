const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
test('sort', () => {
  fc.assert(fc.property(fc.array(fc.integer()), (list) => {
    const sorted = sort(list);
    expect(sorted).toHaveLength(list.length);
    expect(sorted).not.toBe(list);
    expect(sorted).toBeSorted();
  }));
});
// END

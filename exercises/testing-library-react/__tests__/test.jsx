// @ts-check
/* eslint-disable */

import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';
import httpAdapter from 'axios/lib/adapters/http';
import axios from 'axios';

axios.defaults.adapter = httpAdapter;

const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

afterEach(() => {
  nock.cleanAll();
});

// BEGIN
const countries = [
  ['d', ['Djibouti', 'Denmark', 'Dominica', 'Dominican Republic']],
  ['do', ['Dominica', 'Dominican Republic']],
  ['dominica', ['Dominica', 'Dominican Republic']],
  ['dominican', ['Dominican Republic']],
  [' ', []],
];

test.each(countries)('Autocomplete works', async (term, list) => {
  const scope = await nock(host)
    .get('/countries')
    .query({ term })
    .reply(200, list);

  //Второй скоуп для запросов которые не проверяем(пока идет набор текста)
  await nock(host).persist().get(/.*/).optionally().reply(200, []);

  await render(<Autocomplete />);

  const input = await screen.getByRole('textbox');
  await userEvent.type(input, term);

  await waitFor(() => {
    expect(scope.isDone()).toBe(true);
  });
  const ul = await screen.queryByRole('list');

  await list.forEach((i) => {
    expect(ul).toContainElement(screen.getByText(i));
  });

  if (list.length === 0) {
    await expect(ul).not.toBeInTheDocument();
  }
});
// END

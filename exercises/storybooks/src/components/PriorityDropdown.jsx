import 'bootstrap/dist/css/bootstrap.min.css';

import Dropdown from "react-bootstrap/Dropdown";
import { useState } from "react";

export default function PriorityDropdown({ variant, dropDirection, size }) {
  const importance = {
    high: "high",
    middle: "middle",
    low: "low",
  };

  const [state, setState] = useState(importance.middle);

  return (
    <Dropdown drop={dropDirection}>
      <Dropdown.Toggle variant={variant} size={size} id="dropdown-basic">
        {state}
      </Dropdown.Toggle>

      <Dropdown.Menu>
        {Object.keys(importance).map((imp) => (
          <Dropdown.Item
            eventKey={imp}
            key={imp}
            onSelect={setState}
            active={state === imp}
          >
            {imp}
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
}

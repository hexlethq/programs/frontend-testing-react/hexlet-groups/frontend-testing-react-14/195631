const fs = require('fs');
const path = require('path');
const os = require('os');
const { upVersion } = require('../src/index.js');

// BEGIN
const getFixturePath = (filePath) => path.resolve(__dirname, `../__fixtures__/${filePath}`);
const parseFile = (filePath) => JSON.parse(fs.readFileSync(filePath, 'utf8'));

const testFilePath = getFixturePath('package.json');

describe('upVersion', () => {
  let pathToTempFile;
  let currentPackageData;

  beforeEach(() => {
    currentPackageData = parseFile(testFilePath);

    const pathToTempDir = fs.mkdtempSync(path.join(os.tmpdir(), 'upVersion-'));
    pathToTempFile = path.join(pathToTempDir, 'package.json');
    fs.copyFileSync(testFilePath, pathToTempFile);
  });

  it('common', () => {
    upVersion(pathToTempFile);
    const data = parseFile(pathToTempFile);

    expect(data).toHaveProperty('name', 'backend-project-lvl3');
    expect(currentPackageData.version).not.toBe(data.version);
    expect(() => {
      upVersion(pathToTempFile, 'unknown');
    }).toThrow('Unknown version type');
  });

  it.each([
    [undefined, ['0.0.3']],
    ['patch', ['0.0.3', '0.0.4', '0.0.5']],
    ['minor', ['0.1.0']],
    ['major', ['1.0.0', '2.0.0']],
  ])('versionType - %s', (versionType, expects) => {
    /* eslint-disable no-restricted-syntax */
    for (const expected of expects) {
      upVersion(pathToTempFile, versionType);
      const { version } = parseFile(pathToTempFile);
      expect(version).toBe(expected);
    }
  });
});
// END

// @ts-check
/* eslint-disable */

import React from 'react';
import nock from 'nock';
import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

import TodoBox from '../src/TodoBox.jsx';

axios.defaults.adapter = httpAdapter;
const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

afterAll(() => {
  nock.enableNetConnect();
});

// BEGIN
afterEach(() => {
  nock.cleanAll();
});

test('App works', async () => {
  const scope = await nock(host)
    .get('/tasks')
    .reply(200, [{ id: 1, text: 'task1', state: 'active' }]);

  await render(<TodoBox />);

  expect(screen.getByPlaceholderText('I am going...')).toBeVisible();
  expect(screen.getByRole('button', { name: /add/i })).toBeVisible();

  await waitFor(() => {
    expect(scope.isDone()).toBe(true);
    expect(screen.getByText('task1')).toBeVisible();
  });
});

test('Task creating works', async () => {
  const scope = await nock(host)
    .get('/tasks')
    .reply(200, [])
    .post('/tasks', { text: 'task1' })
    .reply(200, { id: 1, text: 'task1', state: 'active' });

  render(<TodoBox />);

  const input = screen.getByPlaceholderText('I am going...');
  const submit = screen.getByRole('button', { name: /add/i });

  userEvent.type(input, 'task1');
  userEvent.click(submit);

  await waitFor(() => {
    expect(scope.isDone()).toBe(true);
    expect(screen.getByText('task1')).toBeVisible();
  });
});

test('Task patching works', async () => {
  const tasks = [
    { id: 1, text: 'task1', state: 'active' },
    { id: 2, text: 'task2', state: 'active' },
  ];

  await nock(host).get('/tasks').reply(200, tasks);

  const scopes = tasks.map((task) => {
    return nock(host)
      .patch(`/tasks/${task.id}/finish`)
      .reply(200, { ...task, state: 'finished' })
      .patch(`/tasks/${task.id}/activate`)
      .reply(200, { ...task, state: 'active' });
  });

  const { container } = render(<TodoBox />);

  userEvent.click(await screen.findByText('task1'));

  await waitFor(() => {
    expect(screen.getByText('task1')).toBeVisible();
    expect(container.querySelectorAll('s')).toHaveLength(1);
  });

  userEvent.click(await screen.findByText('task2'));

  await waitFor(() => {
    expect(screen.getByText('task2')).toBeVisible();
    expect(container.querySelectorAll('s')).toHaveLength(2);
  });

  userEvent.click(await screen.findByText('task1'));
  userEvent.click(await screen.findByText('task2'));

  await waitFor(() => {
    scopes.forEach((scope) => {
      expect(scope.isDone()).toBe(true);
    });
    expect(container.querySelectorAll('s')).toHaveLength(0);
    expect(screen.getByText('task1')).toBeVisible();
    expect(screen.getByText('task2')).toBeVisible();
  });
});
// END

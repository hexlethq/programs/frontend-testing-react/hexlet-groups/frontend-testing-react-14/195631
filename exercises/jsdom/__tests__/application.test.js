// @ts-check
/* eslint-disable */

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');

const run = require('../src/application');

// BEGIN
const elements = {};

beforeEach(() => {
  const initHtml = fs
    .readFileSync(path.join('__fixtures__', 'index.html'))
    .toString();
  document.body.innerHTML = initHtml;
  run();

  elements.lists = document.querySelector('[data-container="lists"]');
  elements.listInput = document.querySelector('[data-testid="add-list-input"]');
  elements.listSubmit = document.querySelector(
    '[data-testid="add-list-button"]'
  );

  elements.tasks = document.querySelector('[data-container="tasks"]');
  elements.taskInput = document.querySelector('[data-testid="add-task-input"]');
  elements.taskSubmit = document.querySelector(
    '[data-testid="add-task-button"]'
  );
});

test('App works', () => {
  expect(elements.lists).toHaveTextContent('General');
  expect(elements.tasks).toBeEmptyDOMElement();
});

test('Task creating works', () => {
  elements.taskInput.value = 'task1';
  elements.taskSubmit.click();

  elements.taskInput.value = 'task2';
  elements.taskSubmit.click();

  expect(elements.taskInput).toBeEmptyDOMElement();
  expect(elements.tasks).toHaveTextContent('task1');
  expect(elements.tasks).toHaveTextContent('task2');
});

test('List creating works', () => {
  elements.taskInput.value = 'task1';
  elements.taskSubmit.click();

  elements.listInput.value = 'New list';
  elements.listSubmit.click();

  const newList = elements.lists.querySelector(
    '[data-testid="new list-list-item"]'
  );
  newList.click();

  expect(elements.tasks).not.toHaveTextContent('task1');

  elements.taskInput.value = 'task2';
  elements.taskSubmit.click();

  expect(elements.tasks).toHaveTextContent('task2');
  expect(elements.lists).toHaveTextContent('New list');
  expect(elements.lists).toHaveTextContent('General');
});
// END

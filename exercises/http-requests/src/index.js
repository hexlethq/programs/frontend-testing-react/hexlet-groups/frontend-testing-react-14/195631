const axios = require('axios');

// BEGIN
function get(url) {
  return axios.get(url).then((res) => res.data);
}

function post(url, data) {
  return axios.post(url, data).then((res) => res.data);
}
// END

module.exports = { get, post };

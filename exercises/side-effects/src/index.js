const fs = require('fs');

// BEGIN
/* eslint-disable implicit-arrow-linebreak */
function upVersion(path, versionType = 'patch') {
  const packageData = JSON.parse(fs.readFileSync(path, 'utf8'));
  const [major, minor, patch] = packageData.version.split('.').map(Number);
  const writeVersion = (version) =>
    fs.writeFileSync(path, JSON.stringify({ ...packageData, version }));

  switch (versionType) {
    case 'major': {
      writeVersion(`${major + 1}.0.0`);
      break;
    }
    case 'minor': {
      writeVersion(`${major}.${minor + 1}.0`);
      break;
    }
    case 'patch': {
      writeVersion(`${major}.${minor}.${patch + 1}`);
      break;
    }
    default: {
      throw new Error(`Unknown version type: ${versionType}`);
    }
  }
}
// END

module.exports = { upVersion };

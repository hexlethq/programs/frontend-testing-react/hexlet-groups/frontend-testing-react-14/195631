const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
nock.disableNetConnect();

const basePath = 'https://example.com';
const userData = {
  firstname: 'Fedor',
  lastname: 'Sumkin',
  age: 33,
};

test('get', async () => {
  const scope = nock(basePath).get('/users').reply(200, userData);

  const responseData = await get(`${basePath}/users`);
  expect(responseData).toEqual(userData);
  scope.done();
});

test('post', async () => {
  const scope = nock(basePath)
    .post('/users', userData)
    .reply(200, userData);

  const data = await post(`${basePath}/users`, userData);
  expect(data).toEqual(userData);
  scope.done();
});
// END
